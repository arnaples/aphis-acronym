"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    "aphis": "Animal and Plant Health Inspection Service",
    "carpol": "Certificates, Accreditations, Registrations, Permits, and Other Licenses",
    "vs": "Veterinary Services",
    "ppq": "Plant Protection and Quarantine",
    "ac": "Animal Care",
    "brs": "Biotechnology Regulatory Services"
};
//# sourceMappingURL=core.js.map