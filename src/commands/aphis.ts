import { SfdxCommand } from '@salesforce/command';
import { Messages, SfdxError } from '@salesforce/core';
import { AnyJson } from '@salesforce/ts-types';
import * as core from '../core';

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('aphis-acronyms', 'aphis');

export default class Aphis extends SfdxCommand{
    public static description = messages.getMessage('aphis.description');
    public static examples = [
        messages.getMessage('aphis.example')
    ];

    protected static requiresUsername = false;
    protected static requiresProject = false;
    public static args = [{name: 'acronym'}]

    public async run(): Promise<AnyJson>{
        let acronym = this.args.acronym;
        
        if(acronym === undefined){
            throw new SfdxError(
                'please enter the acronym you wish to define',
                'noArgSupplied',
                [
                    'sfdx aphis carpol',
                    'sfdx aphis BRS'
                ],
                1
            )
        }
        acronym.toLowerCase();

        let response:string = core.default[acronym];
        if(response === undefined){
            throw new SfdxError(
                `There is no acronym defined for ${acronym}`,
                'unknownAcronym',
                [
                    'sfdx aphis:list'
                ],
                1
            )
        }
        this.ux.log(response);
        return response;
    }
}