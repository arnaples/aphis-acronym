"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const command_1 = require("@salesforce/command");
const core_1 = require("@salesforce/core");
const core = require("../../core");
// Initialize Messages with the current plugin directory
core_1.Messages.importMessagesDirectory(__dirname);
// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = core_1.Messages.loadMessages('aphis-acronyms', 'aphis');
class List extends command_1.SfdxCommand {
    async run() {
        let rows = { results: [] };
        (Object.entries(core.default)).forEach(([key, value]) => rows.results.push({ acronym: key.toUpperCase(), definition: value }));
        this.ux.table(rows.results, ['acronym', 'definition']);
        return rows;
    }
}
exports.default = List;
List.description = messages.getMessage('aphis.list.description');
List.examples = [
    messages.getMessage('aphis.list.example')
];
List.requiresUsername = false;
List.requiresProject = false;
//# sourceMappingURL=list.js.map