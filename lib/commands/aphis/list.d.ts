import { SfdxCommand } from '@salesforce/command';
export default class List extends SfdxCommand {
    static description: string;
    static examples: string[];
    protected static requiresUsername: boolean;
    protected static requiresProject: boolean;
    run(): Promise<any>;
}
