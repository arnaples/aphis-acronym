declare const _default: {
    aphis: string;
    carpol: string;
    vs: string;
    ppq: string;
    ac: string;
    brs: string;
};
export default _default;
