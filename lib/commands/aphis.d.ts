import { SfdxCommand } from '@salesforce/command';
import { AnyJson } from '@salesforce/ts-types';
export default class Aphis extends SfdxCommand {
    static description: string;
    static examples: string[];
    protected static requiresUsername: boolean;
    protected static requiresProject: boolean;
    static args: {
        name: string;
    }[];
    run(): Promise<AnyJson>;
}
