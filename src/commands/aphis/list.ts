import { SfdxCommand } from '@salesforce/command';
import { Messages } from '@salesforce/core';
import * as core from '../../core';

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('aphis-acronyms', 'aphis');

export default class List extends SfdxCommand{
    public static description = messages.getMessage('aphis.list.description');
    public static examples = [
        messages.getMessage('aphis.list.example')
    ];

    protected static requiresUsername = false;
    protected static requiresProject = false;

    public async run(): Promise<any>{
        let rows = {results:[]};
        (Object.entries(core.default)).forEach(([key, value]) => rows.results.push({acronym:key.toUpperCase(), definition:value}))
        this.ux.table(
            rows.results,{}
        );
        return rows;
    }
}