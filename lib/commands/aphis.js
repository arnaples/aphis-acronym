"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const command_1 = require("@salesforce/command");
const core_1 = require("@salesforce/core");
const core = require("../core");
// Initialize Messages with the current plugin directory
core_1.Messages.importMessagesDirectory(__dirname);
// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = core_1.Messages.loadMessages('aphis-acronyms', 'aphis');
class Aphis extends command_1.SfdxCommand {
    async run() {
        let acronym = this.args.acronym;
        if (acronym === undefined) {
            throw new core_1.SfdxError('please enter the acronym you wish to define', 'noArgSupplied', [
                'sfdx aphis carpol',
                'sfdx aphis BRS'
            ], 1);
        }
        acronym.toLowerCase();
        let response = core.default[acronym];
        if (response === undefined) {
            throw new core_1.SfdxError(`There is no acronym defined for ${acronym}`, 'unknownAcronym', [
                'sfdx aphis:list'
            ], 1);
        }
        this.ux.log(response);
        return response;
    }
}
exports.default = Aphis;
Aphis.description = messages.getMessage('aphis.description');
Aphis.examples = [
    messages.getMessage('aphis.example')
];
Aphis.requiresUsername = false;
Aphis.requiresProject = false;
Aphis.args = [{ name: 'acronym' }];
//# sourceMappingURL=aphis.js.map